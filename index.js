//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//Spaghetti Code - when code is not organized enough  that it becomes hard to work it.
// Encapsulation - Organize related information (properties) and behavior (methods) to belong to a single entity.

//Encapsulate the following information into 4 students objects using object literals:
const student1 = {

    name: "John",
    email: "john@mail.com",
    grades: [89, 84, 78, 88], 
    computeAve: function(){
        let sum = this.grades.reduce((accumulator,num) =>{

            console.log(accumulator);//accumulator takes the value of the first item on the first iteration of reduce()

            console.log(num);//num takes the value of the next item.

            return accumulator +=num;
        })
        console.log("result of reduce:",sum, "and the average is:", sum/4);
    },
     willPass: function(){
        return this.computeAve() >=85 ? true : false;
         
    },
     willPassWithHonors: function(){
        return (this.willPass() && Math.round.this.computeAve() >=90) ? true: this.willPass() ?false: undefined;
    },
    login: function(){

        //login should allow our student object to display its own email with our message.
        //What is this in the context of an object method?
        //this when inside a method, refers to the object here it is in.
    
        console.log(`${this.email} has logged in`);
    },
    logout: function(){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    }
    
  };
/* const student2 = {

    name: "Joe",
    email: "joe@mail.com",
    grades: [78, 82, 79, 85],
    login: function(){
        console.log(`${this.email} has logged in`);
    },
    logout: function(){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    }
  };
const student3 = {

    name: "Jane",
    email: "jane@mail.com",
    grades: [87, 89, 91, 93],
    login: function(){

        //login should allow our student object to display its own email with our message.
        //What is this in the context of an object method?
        //this when inside a method, refers to the object here it is in.
    
        console.log(`${this.email} has logged in`);
    },
    logout: function(){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    }
    
  };
const student4 = {

    name: "Jessie",
    email: "jessie@mail.com",
    grades: [91, 89, 92, 93],
    login: function(){

        //login should allow our student object to display its own email with our message.
        //What is this in the context of an object method?
        //this when inside a method, refers to the object here it is in.
    
        console.log(`${this.email} has logged in`);
    },
    logout: function(){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    }
    

  };
console.log(student1) */

/* //create student one
let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93]; */

//actions that students may perform will be lumped together
function login(email){
    console.log(`${email} has logged in`);
}

function logout(email){
    console.log(`${email} has logged out`);
}

function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects


//Activity - Quiz

//1. What is the term to unorganized code that's very hard to work with?
    //Answer: Spaghetti Code

//2. How are object literals written in JS?
    /*Answer: After you declare a variable, inside the object there is a colon separates property name from value.
A comma separates each name-value pair from the next. {}*/ 

//3. What do you call the concept of organizing information and functionality to belong to an object?
    //Answer: encapsulation

//4. If student1 has a method named enroll(), how would you invoke it?
    //Answer: student1.enroll()

//5. True or False: Objects can have objects as properties.
    //Answer: True

//6. What does the this keyword refer to if used in an arrow function method?
    //Answer: always represents the object that defined the arrow function. Global Window Object

//7. True or False: A method can have no parameters and still work.
    //True

//8. True or False: Arrays can have objects as elements.
    //True

//9.  True or False: Arrays are objects.
    //True

//10. True or False: Objects can have arrays as properties.
    //True

//Function Coding

//1. Translate the students from our boilerplate code into their own respective objects:

const student2 = {

    name: "Joe",
    email: "joe@mail.com",
    grades: [78,82,79,85],
    computeAve: function(){
        let sum = this.grades.reduce((accumulator,num) =>{

            console.log(accumulator);

            console.log(num);

            return accumulator +=num;
        })
        console.log("result of reduce:",sum, "and the average is:", sum/4);
    },
    willPass: function(){
        return this.computeAve() >=85 ? true : false;
    },
     willPassWithHonors: function(){
        return (this.willPass() && Math.round.this.computeAve() >=90) ? true: this.willPass() ?false: undefined;
    },
    login: function(){
        console.log(`${this.email} has logged in`);
    },
    logout: function(){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    }
  };
const student3 = {

    name: "Jane",
    email: "jane@mail.com",
    grades: [87, 89, 91, 93],
    computeAve: function(){
        let sum = this.grades.reduce((accumulator,num) =>{

            console.log(accumulator);

            console.log(num);

            return accumulator +=num;
        })
        console.log("result of reduce:",sum, "and the average is:", sum/4);
    },
    willPass: function(){
        return this.computeAve() >=85 ? true : false;
    },
    willPassWithHonors: function(){
        return (this.willPass() && Math.round.this.computeAve() >=90) ? true: this.willPass() ?false: undefined;
    },
    login: function(){

        console.log(`${this.email} has logged in`);
    },
    logout: function(){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    }
    
  };
const student4 = {

    name: "Jessie",
    email: "jessie@mail.com",
    grades: [91, 89, 92, 93],
    computeAve: function(){
        let sum = this.grades.reduce((accumulator,num) =>{

            console.log(accumulator);//accumulator takes the value of the first item on the first iteration of reduce()

            console.log(num);//num takes the value of the next item.

            return accumulator +=num;
        })
        console.log("result of reduce:",sum, "and the average is:", sum/4);
    }, 
    willPass: function(){
     
        return this.computeAve() >=85 ? true : false;
    },
    willPassWithHonors: function(){
       return (this.willPass() && Math.round.this.computeAve() >=90) ? true: this.willPass() ?false: undefined;
    },
  
    login: function(){

        console.log(`${this.email} has logged in`);
    },
    logout: function(){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    }
    
  };

  //2. Define a method for Each student object that will compute for their grade average (total of grades divided by 4)

  //Answer
  /* computeAve: function(){
    let sum = this.grades.reduce((accumulator,num) =>{

        console.log(accumulator);//accumulator takes the value of the first item on the first iteration of reduce()

        console.log(num);//num takes the value of the next item.

        return accumulator +=num;
    })
    console.log("result of reduce:",sum, "and the average is:", sum/4);
},  */
    

  //3. Define a method for all student objects named willPass() that returns a Boolean value indicating if the student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

  //Answer:
  //return this.computeAve() >=85 ? true : false;

 

  //4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than equal to 90, false if >= 85 but <90, and undefined if <85 (since student will not pass).

  //Answer:
  // return (this.willPass() && Math.round.this.computeAve() >=90) ? true: this.willPass() ?false: undefined;
  
    
